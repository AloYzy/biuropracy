﻿<%@ Page Language="C#" MasterPageFile="~/Layout.Master" AutoEventWireup="true" CodeBehind="AddEmployee.aspx.cs" Inherits="BiuroPracy.AddEmployee" %>

<%@ Register TagPrefix="employee" TagName="AddEmployeeControl" Src="~/Controls/AddEmployeeControl.ascx" %>
<%@ Register TagPrefix="employee" TagName="PlaceOfResidenceControl" Src="~/Controls/PlaceOfResidenceControl.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server" />

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server" />

<asp:Content ID="Content3" ContentPlaceHolderID="contentBody" runat="server">
    <h4>Dodawanie pracowników</h4>

    <asp:Panel ID="panelInfo" runat="server">
        <asp:Label ID="labelInfo" runat="server"></asp:Label>
    </asp:Panel>

    <div class="bodyFrom">
        <employee:AddEmployeeControl ID="ctrlAddEmployee" runat="server" />
        <employee:PlaceOfResidenceControl ID="ctrlPlaceOfResidence" runat="server" />

        <div class="form-group">
            <asp:Button ID="saveButton" CssClass="btn-primary" runat="server" Text="Zapisz" />
        </div>

        <div class="form-group">
            <asp:Button ID="testNHibernateButton" runat="server" Text="TestNHibernate" OnClick="testNHibernateButton_Click" />
        </div>
    </div>
</asp:Content>

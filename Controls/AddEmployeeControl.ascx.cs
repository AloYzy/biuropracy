﻿using BiuroPracy.BusinessLogic.Api;
using BiuroPracy.BusinessLogic.Api.Interface;
using BiuroPracy.Domain.Domain;
using System;

namespace BiuroPracy.Controls
{
    public partial class AddEmployeeControl : System.Web.UI.UserControl
    {
        public string Email => emailTextBox.Text;
        public string Password => passwordTextBox.Text;
        public string Name => nameTextBox.Text;
        public string Surname => surnameTextBox.Text;
        public Profession Profession => string.IsNullOrEmpty(professionDropDownList.SelectedValue) ? null :
            new Profession
            {
                Id = Convert.ToInt32(professionDropDownList.SelectedValue),
                ProfessionName = Convert.ToString(professionDropDownList.SelectedItem)
            };
        public Contract Contract => string.IsNullOrEmpty(contractOfEmploymentDropDownList.SelectedValue) ? null :
            new Contract
            {
                Id = Convert.ToInt32(contractOfEmploymentDropDownList.SelectedValue),
                ContractName = Convert.ToString(contractOfEmploymentDropDownList.SelectedItem)
            };
        public DateTime DateOfBirth => dateOfBirthCalendar.SelectedDate;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitControl();
            }
        }

        private void InitControl()
        {
            InitProfession();
            InitContracts();
        }

        private void InitContracts()
        {
            IBiuroPracyApi api = new BiuroPracyApi();

            var dataResponse = api.GetContracts();

            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
                return;
            }

            contractOfEmploymentDropDownList.DataSource = dataResponse.ListOfIdNamePairs;
            contractOfEmploymentDropDownList.DataTextField = "Name";
            contractOfEmploymentDropDownList.DataValueField = "Id";
            contractOfEmploymentDropDownList.DataBind();
        }

        private void InitProfession()
        {
            IBiuroPracyApi api = new BiuroPracyApi();

            var dataResponse = api.GetProfessions();

            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
                return;
            }

            professionDropDownList.DataSource = dataResponse.ListOfIdNamePairs;
            professionDropDownList.DataTextField = "Name";
            professionDropDownList.DataValueField = "Id";
            professionDropDownList.DataBind();
        }

        public void ClearControls()
        {
            emailTextBox.Text = passwordTextBox.Text = nameTextBox.Text = surnameTextBox.Text = string.Empty;
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PlaceOfResidenceControl.ascx.cs" Inherits="BiuroPracy.Controls.PlaceOfResidenceControl" %>

<div class="form-group">
    <label class="control-label col-sm-2">Ulica:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="streetTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <%--// TODO odkomentowac walidacje pol --%>
        <%--<asp:RequiredFieldValidator ID="streetFieldValidator" ControlToValidate="streetTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>--%>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Kod pocztowy:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="zipcodeTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <%--<asp:RequiredFieldValidator ID="zipcodeFieldValidator" ControlToValidate="zipcodeTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>--%>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Miasto:</label>
    <div class="col-sm-10">
        <asp:DropDownList ID="cityDropDownList" CssClass="form-control" runat="server"></asp:DropDownList>
        <%--<asp:RequiredFieldValidator ID="cityFieldValidator" ControlToValidate="cityDropDownList" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>--%>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Kraj:</label>
    <div class="col-sm-10">
        <asp:DropDownList ID="countryDropDownList" CssClass="form-control" runat="server"></asp:DropDownList>
        <%--<asp:RequiredFieldValidator ID="countryFieldValidator" ControlToValidate="countryDropDownList" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>--%>
    </div>
</div>
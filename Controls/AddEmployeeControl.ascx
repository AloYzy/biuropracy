﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddEmployeeControl.ascx.cs" Inherits="BiuroPracy.Controls.AddEmployeeControl" %>

<div class="form-group">
    <label class="control-label col-sm-2">Email:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="emailTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="emailFieldValidator" ControlToValidate="emailTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Hasło:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="passwordTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="passowrdFieldValidator" ControlToValidate="passwordTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Imię:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="nameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="nameFieldValidator" ControlToValidate="nameTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Nazwisko:</label>
    <div class="col-sm-10">
        <asp:TextBox ID="surnameTextBox" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="surnameFieldValidator" ControlToValidate="surnameTextBox" runat="server" ErrorMessage="To pole jest wymagane!"></asp:RequiredFieldValidator>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Data urodzenia:</label>
    <div class="col-sm-10">
        <asp:Calendar ID="dateOfBirthCalendar" runat="server"></asp:Calendar>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Zawód:</label>
    <div class="col-sm-10">
        <asp:DropDownList ID="professionDropDownList" CssClass="form-control" runat="server"></asp:DropDownList>
    </div>
</div>

<div class="form-group">
    <label class="control-label col-sm-2">Umowa o pracę:</label>
    <div class="col-sm-10">
        <asp:DropDownList ID="contractOfEmploymentDropDownList" CssClass="form-control" runat="server"></asp:DropDownList>
    </div>
</div>
﻿using BiuroPracy.BusinessLogic.Api;
using BiuroPracy.BusinessLogic.Api.Interface;
using BiuroPracy.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BiuroPracy.Controls
{
    public partial class PlaceOfResidenceControl : System.Web.UI.UserControl
    {
        public string Street => streetTextBox.Text;
        public string ZipCode => zipcodeTextBox.Text;
        public City City => string.IsNullOrEmpty(cityDropDownList.SelectedValue) ? null :
            new City
            {
                Id = Convert.ToInt32(cityDropDownList.SelectedValue),
                CityName = Convert.ToString(cityDropDownList.SelectedItem)
            };
        public Country Country => string.IsNullOrEmpty(countryDropDownList.SelectedValue) ? null :
            new Country
            {
                Id = Convert.ToInt32(countryDropDownList.SelectedValue),
                CountryName = Convert.ToString(countryDropDownList.SelectedItem)
            };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitControls();
            }
        }

        private void InitControls()
        {
            InitCities();
            InitCountries();
        }

        private void InitCountries()
        {
            IBiuroPracyApi api = new BiuroPracyApi();

            var dataResponse = api.GetCountries();

            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
                return;
            }

            countryDropDownList.DataSource = dataResponse.ListOfIdNamePairs;
            countryDropDownList.DataTextField = "Name";
            countryDropDownList.DataValueField = "Id";
            countryDropDownList.DataBind();
        }

        private void InitCities()
        {
            IBiuroPracyApi api = new BiuroPracyApi();

            var dataResponse = api.GetCities();

            if (!dataResponse.Success)
            {
                Trace.Warn(dataResponse.Errors);
                return;
            }

            cityDropDownList.DataSource = dataResponse.ListOfIdNamePairs;
            cityDropDownList.DataTextField = "Name";
            cityDropDownList.DataValueField = "Id";
            cityDropDownList.DataBind();
        }

        public void ClearControls()
        {
            streetTextBox.Text = zipcodeTextBox.Text = string.Empty;
        }
    }
}
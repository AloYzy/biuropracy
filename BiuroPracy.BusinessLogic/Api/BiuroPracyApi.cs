﻿using BiuroPracy.BusinessLogic.Api.Interface;
using BiuroPracy.BusinessLogic.Communication;
using BiuroPracy.BusinessLogic.Extensions;
using BiuroPracy.BusinessLogic.Logic;
using BiuroPracy.BusinessLogic.ModelDTO;
using BiuroPracy.BusinessLogic.NHibernate;
using BiuroPracy.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Api
{
    public class BiuroPracyApi : BaseApi, IBiuroPracyApi
    {
        public ServiceResponse AddEmployee(EmployeeDto employee)
        {
            using (var session = NHibernateBase.Session)
            {
                using (var transaction = session.BeginTransaction())
                {
                    try
                    {
                        var employeeToAdd = employee.ToEmployee();
                        
                        session.Save(employeeToAdd);
                        transaction.Commit();

                        var email = new EmailManager();
                        email.SendEmail("Dodano nowego pracownika", "hasło: " + employeeToAdd.Password, employeeToAdd.Email);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        return new ServiceResponse(ex);
                    }
                }
            }

            return new ServiceResponse();
        }

        public ListIdNamePairServiceResponse GetCities()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var cities = session.Query<City>()
                                        .Select(c => new IdNamePair { Id = c.Id, Name = c.CityName })
                                        .ToList();

                    return new ListIdNamePairServiceResponse(cities);
                }
            }
            catch (Exception ex)
            {
                return new ListIdNamePairServiceResponse(ex);
            }
        }

        public ListIdNamePairServiceResponse GetContracts()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var contracts = session.Query<Contract>()
                                           .Select(c => new IdNamePair { Id = c.Id, Name = c.ContractName })
                                           .ToList();

                    return new ListIdNamePairServiceResponse(contracts);
                }
            }
            catch (Exception ex)
            {
                return new ListIdNamePairServiceResponse(ex);
            }
        }

        public ListIdNamePairServiceResponse GetCountries()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var countries = session.Query<Country>()
                                           .Select(c => new IdNamePair { Id = c.Id, Name = c.CountryName })
                                           .ToList();

                    return new ListIdNamePairServiceResponse(countries);
                }
            }
            catch (Exception ex)
            {
                return new ListIdNamePairServiceResponse(ex);
            }
        }

        public ListIdNamePairServiceResponse GetProfessions()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    var professions = session.Query<Profession>()
                                             .Select(p => new IdNamePair { Id = p.Id, Name = p.ProfessionName })
                                             .ToList();

                    return new ListIdNamePairServiceResponse(professions);
                }
            }
            catch (Exception ex)
            {
                return new ListIdNamePairServiceResponse(ex);
            }
        }

        public void TestNHibernate()
        {
            try
            {
                using (var session = NHibernateBase.Session)
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        var profession = session.Get<Contract>(1);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}

﻿using BiuroPracy.BusinessLogic.Communication;
using BiuroPracy.BusinessLogic.ModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Api.Interface
{
    public interface IBiuroPracyApi
    {
        void TestNHibernate();
        ListIdNamePairServiceResponse GetProfessions();
        ListIdNamePairServiceResponse GetContracts();
        ListIdNamePairServiceResponse GetCities();
        ListIdNamePairServiceResponse GetCountries();
        ServiceResponse AddEmployee(EmployeeDto employee);
    }
}

﻿using BiuroPracy.BusinessLogic.ModelDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Communication
{
    public class ListIdNamePairServiceResponse : ServiceResponse
    {
        public ListIdNamePairServiceResponse(List<IdNamePair> listOfIdNamePairs) : base()
        {
            ListOfIdNamePairs = listOfIdNamePairs;
        }

        public ListIdNamePairServiceResponse(Exception ex) : base(ex)
        {
        }

        public List<IdNamePair> ListOfIdNamePairs { get; private set; }
    }
}

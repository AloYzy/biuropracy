﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Communication
{
    public class ServiceResponse
    {
        public bool Success { get; private set; }
        public string Errors { get; private set; }


        public ServiceResponse()
        {
            Success = true;
        }

        public ServiceResponse(Exception ex)
        {
            Success = false;
            Errors = ex.StackTrace + " " + ex.Message;
        }
    }
}

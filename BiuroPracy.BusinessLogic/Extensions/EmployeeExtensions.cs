﻿using BiuroPracy.BusinessLogic.ModelDTO;
using BiuroPracy.Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.Extensions
{
    public static class EmployeeExtensions
    {
        public static Employee ToEmployee(this EmployeeDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            var employee = new Employee
            {
                Email = dto.Email,
                Password = dto.Password,
                Name = dto.Name,
                Surname = dto.Surname,
                DateOfBirth = dto.DateOfBirth != null ? dto.DateOfBirth : null,
                Profession = dto.ProfessionId > 0 ? new Profession { Id = dto.ProfessionId } : null,
                Contract = dto.ContractId > 0 ? new Contract { Id = dto.ContractId } : null,
                PlaceOfResidence = new PlaceOfResidence
                {
                    Street = dto.Street,
                    ZipCode = dto.ZipCode,
                    City = dto.CityId > 0 ? new City { Id = dto.CityId } : null,
                    Country = dto.CountryId > 0 ? new Country { Id = dto.CountryId } : null
                }
            };

            return employee;
        }
    }
}

﻿using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BiuroPracy.BusinessLogic.NHibernate
{
    class NHibernateBase
    {
        private static Configuration configuration;// { get; set; }
        private static ISession session;// { get; set; }
        private static IStatelessSession statelessSession;// { get; set; }

        protected static ISessionFactory SessionFactory { get; set; }
        public static ISession Session
        {
            get
            {
                if(session == null || !session.IsOpen)
                {
                    session = SessionFactory.OpenSession();
                }

                return session;
            }
        }
        //=> session.is ?? (session = SessionFactory.OpenSession());
        public static IStatelessSession StatelessSession => statelessSession ?? (statelessSession = SessionFactory.OpenStatelessSession());

        private static Configuration ConfigureNHibernate()
        {
            configuration = new Configuration();
            configuration.Configure();

            return configuration;
        }

        public void Initialize()
        {
            configuration = ConfigureNHibernate();
            SessionFactory = configuration.BuildSessionFactory();
            new SchemaExport(configuration);
        }
    }
}
